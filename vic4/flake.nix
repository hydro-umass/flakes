{
  description = "Variable Infiltration Capacity model";

  inputs.flake-utils.url = "github:numtide/flake-utils";

  outputs = { self, nixpkgs, flake-utils }: 

  flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in  
        rec {
          packages = {
            vic = pkgs.stdenv.mkDerivation {
              name = "vic";
              
              src = pkgs.fetchgit {
                url = "https://github.com/UW-Hydro/VIC.git";
                rev = "VIC.4.2.d";
                sha256 = "sha256-BjxZYe2uR2QrPfr3/N59Kv3vfgrxPgATByLOVbxb6Fk=";
               };
              
              nativeBuildInputs = [ pkgs.gcc pkgs.automake ];
              
              buildPhase = ''
                cd src
                sed -i "s|\/bin\/bash|${pkgs.bash}\/bin\/bash|" Makefile
                make
              '';
              
              # disable stackprotector on aarch64-darwin for now
              # https://github.com/NixOS/nixpkgs/issues/127608
              hardeningDisable = pkgs.lib.optionals (pkgs.stdenv.isAarch64 && pkgs.stdenv.isDarwin) [ "stackprotector" ];
              
              installPhase = ''
                mkdir -p $out/bin
                cp vicNl $out/bin
              '';
            };
         };
         defaultPackage = packages.vic;
      });

}
